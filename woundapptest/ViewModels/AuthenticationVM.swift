//
//  File.swift
//  woundapptest
//
//  Created by Oluwatamilore Adenipekun on 01/01/2022.
//

import Foundation
import FirebaseAuth

class AuthenticationVM: ObservableObject {
    let auth = Auth.auth()
    
    @Published var signedIn : Bool = false
    
    var isSignedIn : Bool {
        return auth.currentUser != nil
    }
    
    func signIn(email:String, password: String){
        auth.signIn(withEmail: email, password: password){[weak self]
            result, error in
            guard result != nil, error == nil
            else {
                self?.signedIn=false
                return
            }
            DispatchQueue.main.async {
                // Success
                self?.signedIn=true
                print(self?.isSignedIn)
            }
            
        }
        
    }
    
    
    func signUp(email:String, password: String){
        auth.createUser(withEmail: email, password: password){[weak self]
            result, error in
            guard result != nil , error == nil
            else{
                return
            }
            DispatchQueue.main.async {
                // Success
                self?.signedIn=true
            }
        }
    }
    
    func signOut(){
        try? auth.signOut()
        signedIn.toggle()
    }
    
}
