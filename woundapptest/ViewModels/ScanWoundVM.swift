//
//  ScanWoundVM.swift
//  woundapptest
//
//  Created by Aaditya Singh on 02/03/22.
//

import Foundation
import UIKit
import Firebase
import PhotosUI
import AVKit

class ScanWoundVM: ObservableObject {
    
    
    @Published var importFile = false
    @Published var recordVideo = false
    
    //for showing the list of videos
    @Published var isSheetPresented = false
    @Published var videos = [URL]()
    @Published var sheetMode: SheetMode = .picker
    @Published var player: AVPlayer?

    enum SheetMode {
      case picker
      case video
    }
    
    @Published var showVideoListView = false
    @Published var showImagePicker = false
    @Published var source: Picker.Source = .library
    @Published var showCameraAlert = false
    @Published var cameraError: Picker.CameraErrorType?

    
   
    
    func showPhotoPicker() {
        do {
            if source == .camera {
                try Picker.checkPermission()
            }
            showImagePicker = true

        }
        catch {
            showCameraAlert = true
            cameraError = Picker.CameraErrorType(error: error as! Picker.PickerError)
        }
    }
  
}
