//
//  SaveImagesVM.swift
//  woundapptest
//
//  Created by Oluwatamilore Adenipekun on 06/03/2022.
//

import Foundation
import FirebaseAuth
import Firebase

class SaveImagesVM: ObservableObject {
    let auth = Auth.auth()
    
    @Published var image: UIImage?
    
    //Saving Images to Firebase
    func persistImageToStorage() {
        guard let uid = auth.currentUser?.uid else {return}
        //let ref =  Storage.storage().reference(withPath: uid)
        let ref =  Storage.storage().reference()
        guard let imageData = self.image?.pngData() else {return}
        let tempName = ref.child("woundImages/file-\(uid).png")
        tempName.putData(imageData, metadata: nil) { metaData, error in
            if let error = error {
                print("Failed to push image to storage \(error.localizedDescription)")
                return
            }
            tempName.downloadURL {  url, err in
                if let err = err {
                    print(err)
                }
                print("successfully stored image \(url?.absoluteString ?? "")")
            }
        }
    }
    
    //Saving Videos to Firebase
    
    func saveVideo(url: URL?) {
        guard let videoUrl = url else { return }
                
                let videoName = NSUUID().uuidString
                let storageRef = Storage.storage().reference().child("\(videoName).mov")
                storageRef.putFile(from: videoUrl as URL, metadata: nil) { (metaData, error) in
                     // IMPORTANT: this is where I got the error from
                    if error != nil {
                        print("error uploading video: \(error!.localizedDescription)")
                    } else {
                        // successfully uploaded the video
                        storageRef.downloadURL { (url, error) in
                            if error != nil {
                                print("error downloading uploaded videos Url: \(error!.localizedDescription)")
                            } else {
                                if let downloadUrl = url {
                                    let contentType = "videoUrl" //just a var for the following func
//                                    self.uploadPost(for: downloadUrl, contentType: contentType) // func that uploads the url to the database
                                }
                            }
                        }
                    }
                }
    }
}
