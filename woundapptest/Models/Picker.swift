
//  Picker.swift
//  woundapptest
//
//  Created by Aaditya Singh on 02/03/22.
//

import AVFoundation
import SwiftUI

enum Picker {
    enum Source {
        case library, camera
    }
    
    enum PickerError: Error, LocalizedError {
        case unavailable
        case restricted
        case denied
        
        var errorDescription: String? {
            switch self {
            case .unavailable:
                return NSLocalizedString("There is no available camera ", comment: "")
            case .restricted:
                return NSLocalizedString("You are not allowed to access media capture devices", comment: "")
            case .denied:
                return NSLocalizedString("You have denied permission for media capture. Please open permissions/Privacy/Camera and grant access for this application", comment: "")

            }
        }
    }
    
    static func checkPermission()throws {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let mediaStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
            switch mediaStatus {
            case .restricted:
                throw PickerError.restricted
            case .denied:
                throw PickerError.denied
            default:
                break
           
            }
            
        } else {
            throw PickerError.unavailable
        }
    
    }
    
    struct CameraErrorType {
        let error: Picker.PickerError
        var message: String {
            error.localizedDescription
        }
        let button = Button("OK", role: .cancel) {}
    }
    
}
