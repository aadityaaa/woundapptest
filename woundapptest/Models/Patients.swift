//
//  Patients.swift
//  woundapptest
//
//  Created by Oluwatamilore Adenipekun on 16/01/2022.
//

import Foundation
import FirebaseFirestoreSwift
import SwiftUI

struct Patients: Identifiable, Codable {
    @DocumentID  var id: String? = UUID().uuidString
    var firstName: String
    var lastName: String
    var middleName: String
    var gender: String
    var height: String //TO-DO chabge type to double
    var dateOfBirth: String // Figure out how to allow user to select from calender picker
    var weight: String // change type to float
    var MRN: String
    var phone: String
    var email: String
    var address: String
    var emergencyContact: String
    var sepsisSurvey:[Bool]
    var bradenScore:String
    var bs:[String]
    var patientHistory:[String]
    var wounds:[Wound]
    
    enum CodingKeys: String, CodingKey{
        case firstName = "First Name"
        case lastName = "Last Name"
        case middleName = "Middle Name"
        case gender = "Gender"
        case height = "Height"
        case dateOfBirth = "Birth Date"
        case weight = "Weight"
        case MRN = "MRN"
        case phone = "Phone"
        case email = "Email"
        case address = "Address"
        case emergencyContact = "Emergency Contact"
        case sepsisSurvey = "Sepsis Survey"
        case bradenScore = "Braden Score Total"
        case bs = "Braden Scores"
        case patientHistory = "Patient History"
        case wounds = "Wounds"
    }
}


//struct Patients: Identifiable, Codable {
//    @DocumentID  var id: String? = UUID().uuidString
//    var firstName: String
//    var lastName: String
//    var middleName: String
//    var gender: String
//    var height: String //TO-DO chabge type to double
//    var dateOfBirth: String // Figure out how to allow user to select from calender picker
//    var weight: String // change type to float
//
//    enum CodingKeys: String, CodingKey{
//        case firstName = "First Name"
//        case lastName = "Last Name"
//        case middleName = "Middle Name"
//        case gender = "Gender"
//        case height = "Height"
//        case dateOfBirth = "Birth Date"
//        case weight = "Weight"
//    }
//}
