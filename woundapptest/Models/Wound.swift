//
//  Wound.swift
//  woundapptest
//
//  Created by Carlos Morales III on 3/31/22.
//

import Foundation
import FirebaseFirestoreSwift

struct Wound: Identifiable, Codable {
	@DocumentID var id: String? = UUID().uuidString
	var woundName: String
	var stage: String
	var position: String
	
	enum CodingKeys: String, CodingKey{
		case woundName = "Wound Name"
		case stage = "Stage"
		case position = "Position"
	}
}
