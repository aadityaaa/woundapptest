//
//  ScanWound.swift
//  woundapptest
//
//  Created by Oluwatamilore Adenipekun on 29/01/2022.
//

import SwiftUI
import PhotosUI
import AVKit

struct ScanWound: View {
    
    @StateObject var viewmodel = ScanWoundVM()
    @EnvironmentObject var saveimagesvm :SaveImagesVM
    
    @State var importFile = false
    var body: some View {
        VStack{
            if let image = self.saveimagesvm.image
            {
                VStack{
                    Image(uiImage: image)
                        .resizable()
                        .scaledToFill()
                        .frame(width: 256, height: 256)
                        .cornerRadius(15)
                   NavigationLink(destination: WoundHomeAssignment(), label:{
//                    Button(action: { saveimagesvm.persistImageToStorage()
//                    },
//                           label: {
                                    MediumButton(text: "Upload Image", fgcolor: "Background",
                                                 bgcolor: "Primary", width: 160, height: 80)
                        })
                }
            }
            else {
                Image(systemName: "photo.fill")
                    .font(.system(size: 64))
                    .padding()
                    .foregroundColor(Color("Primary"))
            }
            
            //to preview videos
            
            Button(action: {viewmodel.showVideoListView.toggle()}, label: {
                    MediumButton(text: "Preview Video", fgcolor: "Background", bgcolor:"Primary", width: 160, height: 50)
                })
            
            //to take a video
            Button(action: {viewmodel.recordVideo.toggle()}, label: {
                    MediumButton(text: "Record Video", fgcolor: "Background", bgcolor:"Primary", width: 160, height: 50)
                })
            
            //To take a picture

            Button(action: {
                viewmodel.source = .camera
                viewmodel.showPhotoPicker()},
                   label: {
                            MediumButton(text: "Scan Wound", fgcolor: "Background",
                                         bgcolor: "Primary", width: 160, height: 50)
                        })

            //To Import Images from Library
            Button(action: {
                viewmodel.source = .library
                viewmodel.showPhotoPicker()}, label: {
                    MediumButton(text: "Import from library", fgcolor: "Background",
                                 bgcolor: "Primary", width: 160, height: 50)
                })
            
            //To Import Images from Files
            Button(action: {viewmodel.importFile.toggle()}, label: {
                    MediumButton(text: "Import from Files", fgcolor: "Background", bgcolor:"Primary", width: 160, height: 50)
                })
            
        }.fileImporter(isPresented: $viewmodel.importFile, allowedContentTypes: [.image,.jpeg,.heic,.heif,.usd,.usdz,.threeDContent]) { result in
            do
            {
                let fileurl = try result.get()
                print(fileurl)
            }
            catch{
                print("error reading file")
                print(error.localizedDescription)
            }
        }
        .sheet(isPresented: $viewmodel.showImagePicker, onDismiss: nil) {
            ImagePicker(sourceType: viewmodel.source == .library ? .photoLibrary: .camera, image:$saveimagesvm.image)
                .ignoresSafeArea()
        }
        .sheet(isPresented: $viewmodel.recordVideo, onDismiss: nil) {
                RecordVideoView()
                .ignoresSafeArea()
        }
        .sheet(isPresented: $viewmodel.showVideoListView, onDismiss: nil) {
            VideoListView
            .ignoresSafeArea()
        }
        .alert("Error", isPresented: $viewmodel.showCameraAlert, presenting: viewmodel.cameraError, actions: {
            cameraError in
            cameraError.button
        }, message: { cameraError in
            Text(cameraError.message)
        })
    }
}


struct ScanWound_Previews: PreviewProvider {
    static var previews: some View {
        ScanWound()
            .environmentObject(SaveImagesVM())
    }
}

extension ScanWound {
    
    //this is the video list view
    private var VideoListView: some View{
      VStack {
        List {
            ForEach(0 ..< viewmodel.videos.count, id: \.self) { index in
            HStack {
                HStack {
              Thumbnail(url: viewmodel.videos[index])
              Text("\(index + 1)")
                }
                .onTapGesture {
                  viewmodel.isSheetPresented = true
                  viewmodel.sheetMode = .video
                  viewmodel.player = AVPlayer(url: viewmodel.videos[index])
                }
                Button(action: {saveimagesvm.saveVideo(url: viewmodel.videos[index])}, label: {
                    MediumButton(text: "Upload Video", fgcolor: "Background", bgcolor:"Primary", width: 120, height: 40)
                })
            }
           

          }
        }
        HStack {
          HStack {
            Button {
                viewmodel.isSheetPresented = true
            } label: {
              Image(systemName: "video.badge.plus")
                .font(.largeTitle)
            }
          }
          .padding(.leading)
//          Spacer()
        }
        .sheet(isPresented: $viewmodel.isSheetPresented) {
            switch(viewmodel.sheetMode) {
          case .picker:
                PhotoPicker(isPresented: $viewmodel.isSheetPresented, videos: $viewmodel.videos)
          case .video:
                VideoPlayer(player: viewmodel.player)
          }

        }
      }
   
    }
}
