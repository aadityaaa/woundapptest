//
//  ViewWound.swift
//  woundapptest
//
//  Created by Oluwatamilore Adenipekun on 29/01/2022.
//

import SwiftUI
import SceneKit

struct WoundHomeAssignment: View {
	
	init() {
		UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: UIColor(named: "Title") ?? .black]
	}
	
    var body: some View {
			VStack {
				HStack {
					Spacer()
					
					NavigationLink(destination: WoundLocationAssignment(), label: {
						Text("\(Image(systemName: "plus"))  Add new wound")
							.font(.system(size: 14))
						
					})
					.padding()
					.background(Color("Icon_Scanwound"))
					.foregroundColor(.white)
					.cornerRadius(8)
					.frame(width: 165, height: 40, alignment: .center)
					.padding(.trailing, 10)
					.padding(.bottom, 25)
				}
				
				Group {
					VStack {
						HStack {
							WoundDescriptionBubble()
							WoundDescriptionBubble()
						}
						HStack {
							WoundDescriptionBubble()
							WoundDescriptionBubble()
						}
					}
				}
				.padding(.bottom, 60)
				
				Image("Wound Diagram")
					.frame(alignment: .center)
					.padding(.bottom,10)
			}
	}
}

struct WoundHomeAssignment_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			WoundHomeAssignment()
				.previewDevice(PreviewDevice(rawValue: "iPhone 8 Plus"))
			WoundHomeAssignment()
				.previewDevice(PreviewDevice(rawValue: "iPhone 12 Pro"))
		}
	}
}
