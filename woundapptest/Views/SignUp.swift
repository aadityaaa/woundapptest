//
//  SignUp.swift
//  woundapptest
//
//  Created by Oluwatamilore Adenipekun on 26/12/2021.
//

import SwiftUI

struct SignUp: View {
    @Binding var username: String
    @Binding var password: String
    @State var email: String = ""
    
    @EnvironmentObject var signinviewmodel: AuthenticationVM
    
    var body: some View {
        ZStack {
            Color("Primary").ignoresSafeArea()
            VStack
            {
                LogoWithName()
                TextField("Hospital ID", text: $username)
                    .frame(width: 280, height: 25, alignment: .center)
                    .padding()
                    .background()
                    .cornerRadius(5.0)
                    .padding(.bottom, 20)
                
                TextField("E-mail", text: $email)
                    .frame(width: 280, height: 25, alignment: .center)
                    .padding()
                    .background()
                    .cornerRadius(5.0)
                    .padding(.bottom, 20)
                
                SecureField("Password", text: $password)
                    .frame(width: 280, height: 25, alignment: .center)
                    .padding()
                    .background()
                    .cornerRadius(5.0)
                    .padding(.bottom,30)
                
                NavigationLink(isActive: $signinviewmodel.signedIn, destination:{HomeScreen()}, label: {Button(action: {
                guard !username.isEmpty, !password.isEmpty else{
                    return
                }
                    signinviewmodel.signUp(email: username, password: password)
                }, label: {
                    SignupButton()
                    
                })})
//                Button(action: {print("Sign up Button tapped")})
//                {
//                    SignupButton()
//                }.padding(.horizontal, 40)
                Text("Need help?")

                    .font(.footnote)
            }
        }
    }
}

struct SignUp_Previews: PreviewProvider {
    @Binding var username: String
    @Binding var password: String
    static var previews: some View {
        SignUp(username: .constant(""), password: .constant(""))
            .environmentObject(AuthenticationVM())
    }
}
