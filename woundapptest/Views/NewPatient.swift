//
//  NewPatient.swift
//  woundapptest
//
//  Created by Oluwatamilore Adenipekun on 31/12/2021.
//

import SwiftUI

struct NewPatient: View {

    @State var firstName:String = ""
    @State var lastName:String = ""
    @State var bd: String = ""
    @State var sex:String = ""
    @State var height:String = ""
    @State var weight:String = ""
    @State var address:String = ""
    @State var phone:String = ""
    @State var allergies:String = ""
    @State var wound:String = ""
    @State var mrn:String = ""
    
    var body: some View {
        VStack {
            Circle().foregroundColor(Color(.systemGray6)).frame(width: 100, height: 100).padding(.bottom,40)
            VStack (alignment:.leading){
                HStack{
                    TextField("*First Name", text: $firstName)
                        .frame(width: 140, height: 15, alignment: .center)
                        .padding()
                        .background(Color(.systemGray6))
                        .cornerRadius(5.0)
                    TextField("*Last Name", text: $lastName)
                        .frame(width: 140, height: 15, alignment: .center)
                        .padding()
                        .background(Color(.systemGray6))
                        .cornerRadius(5.0)
                }.padding(.bottom, 20)
                HStack{
                    TextField("*Date of Birth", text: $bd)
                        .frame(width: 140, height: 15, alignment: .center)
                        .padding()
                        .background(Color(.systemGray6))
                        .cornerRadius(5.0)
                    TextField("*Sex", text: $sex)
                        .frame(width: 140, height: 15, alignment: .center)
                        .padding()
                        .background(Color(.systemGray6))
                        .cornerRadius(5.0)
                }.padding(.bottom,20)
                HStack{
                    TextField("*Height", text: $height)
                        .frame(width: 140, height: 15, alignment: .center)
                        .padding()
                        .background(Color(.systemGray6))
                        .cornerRadius(5.0)
                    TextField("*Weight", text: $weight)
                        .frame(width: 140, height: 15, alignment: .center)
                        .padding()
                        .background(Color(.systemGray6))
                        .cornerRadius(5.0)
                }.padding(.bottom,20)
                HStack{
                    TextField("*Address", text: $address)
                        .frame(width: 140, height: 15, alignment: .center)
                        .padding()
                        .background(Color(.systemGray6))
                        .cornerRadius(5.0)
                    TextField("*Phone number", text: $phone)
                        .frame(width: 140, height: 15, alignment: .center)
                        .padding()
                        .background(Color(.systemGray6))
                        .cornerRadius(5.0)
                }.padding(.bottom,20)
                HStack{
                    TextField("*Allergies", text: $allergies)
                        .frame(width: 140, height: 15, alignment: .center)
                        .padding()
                        .background(Color(.systemGray6))
                        .cornerRadius(5.0)
                    TextField("*Wound", text: $wound)
                        .frame(width: 140, height: 15, alignment: .center)
                        .padding()
                        .background(Color(.systemGray6))
                        .cornerRadius(5.0)
                }.padding(.bottom,20)
                TextField("*MRN", text: $mrn)
                    .frame(width: 140, height: 15, alignment: .leading)
                    .padding()
                    .background(Color(.systemGray6))
                    .cornerRadius(5.0)
            }.padding(.bottom,80)
            HStack(spacing:20){
                Group {
                    Button {
                        
                    } label: {
                        Text("Clear All").foregroundColor(Color("Icon_Patients")).frame(width: 100, height: 10, alignment: .center).padding().overlay(
                            RoundedRectangle(cornerRadius: 5)
                                .stroke(Color("Icon_Patients"), lineWidth: 1)
                        )
                    }
                }.background(.white).cornerRadius(5).padding(5)
                Group {
                    Button {
                        
                    } label: {
                        Text("Save").foregroundColor(.white).frame(width: 100, height: 10, alignment: .center).padding()
                    }
                }.background(Color("Icon_Patients")).cornerRadius(5).padding(5)
            }
        }.padding()
    }
}

struct NewPatient_Previews: PreviewProvider {
    static var previews: some View {
        NewPatient()
    }
}
