//
//  WoundSubLocationAssignment.swift
//  woundapptest
//
//  Created by Carlos Morales III on 3/10/22.
//

import SwiftUI

struct WoundSubLocationAssignment: View {
	@ObservedObject private var keyboard = KeyboardResponder()
	@State private var additionalLocationDetails: String = ""
	
	init() {
		UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: UIColor(named: "Title") ?? .black]
	}
	
    var body: some View {
			VStack {
				Image("anatomicalArea")
					.frame(maxWidth: 176, maxHeight: 350)
					.padding(.top, 18)
					.fixedSize()
				
				DropdownMenu()
					.padding(.trailing, 20)
					.padding(.leading, 197)
					.padding(.bottom, -40)
				
				TextField("Additional location details", text: $additionalLocationDetails)
					.textFieldStyle(.roundedBorder)
					.frame(width: 315, height: 100, alignment: .center)
					.submitLabel(.done)
				
				
				HStack {
					Button(action: {}, label: { Text("Cancel") })
						.frame(width: 70, height: 36, alignment: .center)
						.background(.white)
						.foregroundColor(Color("Icon_Scanwound"))
						.overlay(RoundedRectangle(cornerRadius: 5).stroke(Color("Icon_Scanwound"), lineWidth: 1))
					Button(action: {}, label: { Text("Next") })
						.frame(width: 70, height: 36, alignment: .center)
						.background(Color("Icon_Scanwound"))
						.foregroundColor(.white)
						.cornerRadius(5)
				}
			}
			.padding(.bottom, keyboard.currentHeight)
			.edgesIgnoringSafeArea(.bottom)
			.animation(.easeOut, value: 0.16)

		}
}

struct WoundSubLocationAssignment_Previews: PreviewProvider {
    static var previews: some View {
		Group {
			WoundSubLocationAssignment()
				.previewDevice(PreviewDevice(rawValue: "iPhone 8 Plus"))
			WoundSubLocationAssignment()
				.previewDevice(PreviewDevice(rawValue: "iPhone 12 Pro"))
		}
    }
}
