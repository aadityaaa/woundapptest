//
//  Splash Screen.swift
//  woundapptest
//
//  Created by Oluwatamilore Adenipekun on 24/12/2021.
//

import SwiftUI

struct SplashScreen: View {
    
    @State private var isActive: Bool = false
    @EnvironmentObject var signinviewmodel : AuthenticationVM
    var body: some View {
        
        if self.isActive {
            SignIn()
        } else {
            ZStack
            {
                Color("Primary")
                VStack{
                
                    Image("KOS_LogoTransparent").resizable(resizingMode:.stretch).frame(maxWidth:200,maxHeight: 200)
                        .background(Color("Primary")).foregroundColor(Color("Primary"))
                    
                    Text("KOS").font(.largeTitle)
                        .foregroundColor(.white)
                }
            }.ignoresSafeArea()
                .onAppear{
                    DispatchQueue.main.asyncAfter(deadline: .now()+2.5)
                    {withAnimation {
                        self.isActive = true
                        
                    }
                }
            }
        }
        
    }
}

struct SplashScreen_Previews: PreviewProvider {
    static var previews: some View {
        SplashScreen()
.previewInterfaceOrientation(.portrait)
    }
}
