//
//  HomeScreen.swift
//  woundapptest
//
//  Created by Oluwatamilore Adenipekun on 31/12/2021.
//

import SwiftUI

struct HomeScreen: View {
    @EnvironmentObject var signinstatevm :AuthenticationVM
    
    var body: some View {
        VStack {

            ProfilePic()
            
            
            HomeScreenGrid()
            
        }.navigationTitle(Text("Home"))
            .navigationBarBackButtonHidden(true)
            .toolbar{
                ToolbarItemGroup(placement: .navigationBarTrailing){
                    Button(action: signinstatevm.signOut){
                        Text("Sign Out")
                        
                    }//change to button where action will call signout functiion
                }
           //.fullScreenCover(isPresented: $signinstatevm.signedIn, onDismiss: nil){
//                SignIn()
            }
    }
}

struct HomeScreen_Previews: PreviewProvider {
    static var previews: some View {
        HomeScreen()
            .environmentObject(AuthenticationVM())
.previewInterfaceOrientation(.portrait)
    }
}

struct HomeScreenGrid: View {
    let layout = [GridItem(.flexible(maximum:120)),
                  GridItem(.flexible(maximum: 120))]
    var body: some View {
        
        VStack {
            LazyHGrid(rows: layout, spacing: 20)
            {
                NavigationLink(destination: PatientsList(), label: {
                    HomescreenButton(text:"PATIENTS",iconname:"person.2.circle.fill", iconforegroundcolor:"Icon_Patients")
                }
                )
                NavigationLink(destination: ScanWound(), label:{
                HomescreenButton(text: "SCAN WOUND", iconname: "camera.circle.fill", iconforegroundcolor: "Icon_Scanwound")
                }
                )
                NavigationLink(destination: NewPatient(), label: {
                HomescreenButton(text:"NEW PATIENTS",iconname:"person.crop.circle.fill.badge.plus", iconforegroundcolor:"Primary")
                }
                )
                NavigationLink(destination: NewPatient(), label: {
                HomescreenButton(text: "SETTINGS", iconname: "gearshape.circle.fill", iconforegroundcolor: "Icon_Settings")
                }
                               )
            }.padding()
            ScanQRButton()
        }
    }
}

struct ProfilePic: View {
    var body: some View {
        VStack {
            Image("PaulSmith")
                .resizable()
                .frame(width: 61, height: 61)
                .clipShape(Circle())
            Text("Dr Paul Smith")
                .padding()
        }
    }
}
