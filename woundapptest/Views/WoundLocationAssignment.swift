//
//  WoundLocationAssignment.swift
//  woundapptest
//
//  Created by Carlos Morales III on 3/10/22.
//

import SwiftUI

struct WoundLocationAssignment: View {
	@State var currentSelectedRegionName: String? = nil
	@State var currentSelectedHemisphereName: String? = nil
	@State var currentSelectedSideName: String? = nil
	
	let regionOptions: [String] = ["Anterior", "Side", "Posterior"]
	let hemisphereOptions: [String] = ["Upper", "Lower"]
	let sideOptions: [String] = ["Left", "Right"]
	
	let regionButtonWidth: CGFloat = 101.5
	let regionButtonHeight: CGFloat = 40
	
	let hemisphereButtonWidth: CGFloat = 160
	let hemisphereButtonHeight: CGFloat = 40
	
	let sideButtonWidth: CGFloat = 160
	let sideButtonHeight: CGFloat = 40
	
	let navigationButtonWidth: CGFloat = 70
	let navigationButtonHeight: CGFloat = 36
	
	let cornerRadius: CGFloat = 5.0
	
	init() {
		/* This initializer enables us to directly interface with changes to the NavBar's title properties. This can be modularized to keep overhead at a minimum, since other views also make use of this color scheme.
		 */
		
		UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: UIColor(named: "Title") ?? .black]
	}
	
	var body: some View {
			VStack {
				Text("Select one from each category")
					.foregroundColor(.gray)
					.padding(.trailing, 160)
					.padding(.leading, 28)
					.font(.system(size: 12))
				
				// First row of buttons (Region)
				HStack {
					ForEach(regionOptions, id: \.self) { region in
						if region != currentSelectedRegionName { // Output white button
							Button(
								action: { currentSelectedRegionName = region },
								label: { Text(region) })
							.padding()
							.frame(width: regionButtonWidth, height: regionButtonHeight)
							.overlay(RoundedRectangle(cornerRadius: cornerRadius)
								.stroke(Color("Primary")))
						} else { // Output blue (selected) button
							Button(
								action: {},
								label: { Text(region) })
							.padding()
							.frame(width: regionButtonWidth, height: regionButtonHeight)
							.foregroundColor(Color.white)
							.background(Color("Primary"))
							.cornerRadius(cornerRadius)
						}
					}
				}
				.padding(.bottom)
				
				// Upper / Lower Selection
				HStack {
					ForEach(hemisphereOptions, id: \.self) { hemisphere in
						if hemisphere != currentSelectedHemisphereName {
							Button(
								action: { currentSelectedHemisphereName = hemisphere },
								label: { Text(hemisphere) })
							.padding()
							.frame(width: hemisphereButtonWidth, height: hemisphereButtonHeight)
							.overlay(
								RoundedRectangle(cornerRadius: cornerRadius)
									.stroke(Color("Primary")))
						} else {
							Button(
								action: {},
								label: { Text(hemisphere) })
							.padding()
							.frame(width: hemisphereButtonWidth, height: hemisphereButtonHeight)
							.foregroundColor(Color.white)
							.background(Color("Primary"))
							.cornerRadius(cornerRadius)
						}
					}
				}
				.padding(.bottom)
				
				// Side Selection
				HStack {
					ForEach(sideOptions, id: \.self) { side in
						if side != currentSelectedSideName {
							Button(
								action: { currentSelectedSideName = side },
								label: { Text(side) })
							.padding()
							.frame(width: sideButtonWidth, height: sideButtonHeight)
							.overlay(
								RoundedRectangle(cornerRadius: cornerRadius)
									.stroke(Color("Primary")))
						} else {
							Button(
								action: {},
								label: { Text(side) })
							.padding()
							.frame(width: sideButtonWidth, height: sideButtonHeight)
							.foregroundColor(Color.white)
							.background(Color("Primary"))
							.cornerRadius(cornerRadius)
						}
					}
				}
				.padding(.bottom)
				
				// Navigation Options Selection
				HStack {
					Spacer()
					Button(action: {}, label: { Text("Cancel") })
						.frame(width: navigationButtonWidth, height: navigationButtonHeight, alignment: .center)
						.foregroundColor(Color("Icon_Scanwound"))
						.overlay(
							RoundedRectangle(cornerRadius: cornerRadius).stroke(Color("Icon_Scanwound")))
					
					NavigationLink(destination: WoundSubLocationAssignment(), label: { Text("Next") })
						.padding()
						.frame(width: navigationButtonWidth, height: navigationButtonHeight, alignment: .center)
						.foregroundColor(.white)
						.background(Color("Icon_Scanwound"))
						.cornerRadius(cornerRadius)
				}
				.padding(.bottom, 40)
				.padding(.trailing, 50)
                
				
				Image("Wound Diagram")
			}
			.padding(.bottom, 10)
		}
}

struct WoundLocationAssignment_Previews: PreviewProvider {
    static var previews: some View {
		Group {
			WoundLocationAssignment()
				.previewDevice(PreviewDevice(rawValue: "iPhone 8 Plus"))
			WoundLocationAssignment()
				.previewDevice(PreviewDevice(rawValue: "iPhone 12 Pro"))
		}
    }
}
