//
//  PatientProfile.swift
//  woundapptest
//
//  Created by stella on 2/28/22.
//

import SwiftUI

struct PatientProfile: View {
    
    @State var opacity = 1.0
    @State var show:Bool = false
    @State var newScore:Bool = false
    var patient:Patients
    @State var expand = [false,false,false,false,false,false,false,false]
    
    var body: some View {
        //NavigationView {
            ZStack {
                if (show || newScore){
                    Color.gray.ignoresSafeArea()
                }
                else{
                    Color.white.ignoresSafeArea()
                }
                ScrollView {
                    VStack{
                        Profile(patient:patient)
                        VStack(spacing:30){
                            VStack{
                                ExpandInfo(expanding: $expand[0], information: "Contact Details")
                                if expand[0]{
                                    ContactDetails(patient: patient).padding()
                                }//.padding(3)
                            }.onTapGesture {
                                expand[0].toggle()
                            }
                            VStack{
                                ExpandInfo(expanding: $expand[1], information: "Patient History")
                                if expand[1]{
                                    PatientHistory(patient:patient)
                                }
                            }.onTapGesture {
                                expand[1].toggle()
                            }
                            VStack{
                                ExpandInfo(expanding: $expand[2], information: "Wounds")
                                if expand[2]{
                                    Wounds(patient:patient)//.frame(maxWidth: .infinity, alignment: .center)
                                }
                            }.onTapGesture {
                                expand[2].toggle()
                            }
                            VStack{
                                ExpandInfo(expanding: $expand[3], information: "Braden Score")
                                if expand[3]{
                                    BradenScore(patient: patient,newScore: $newScore,opacity:$opacity)
                                }
                            }.onTapGesture {
                                expand[3].toggle()
                            }
                            VStack{
                                ExpandInfo(expanding: $expand[4], information: "Daily Check-in")
                                if expand[4]{
                                    DailyCheckIn(show: $show)
                                }
                            }.onTapGesture {
                                expand[4].toggle()
                            }
                            VStack{
                                ExpandInfo(expanding: $expand[5], information: "Tests")
                                if expand[5]{
                                    Test()
                                }
                            }.onTapGesture {
                                expand[5].toggle()
                            }
                            VStack{
                                ExpandInfo(expanding: $expand[6], information: "Additional information")
                                if expand[6]{
                                    
                                }
                            }.onTapGesture {
                                expand[6].toggle()
                            }
                            VStack{
                                ExpandInfo(expanding: $expand[7], information: "Treatment")
                                if expand[7]{
                                    
                                }
                            }.onTapGesture {
                                expand[7].toggle()
                            }
                        }
                    }
                    
                }.opacity(opacity)
                SepsisSurvey(show: $show,patient: patient)
                BradenFormComplete(newScore: $newScore,opacity:$opacity)
            }.navigationBarTitle("",displayMode: .inline)
        //.navigationBarHidden(true)
        //}
    }
}
struct PatientProfile_Previews: PreviewProvider {
    static var previews: some View {
        PatientProfile(patient: testData[0])
    }
}
