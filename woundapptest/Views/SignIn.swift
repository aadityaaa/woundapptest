//
//  SignIn.swift
//  woundapptest
//
//  Created by Oluwatamilore Adenipekun on 24/12/2021.
//

import SwiftUI

struct SignIn: View {
    
    @State var username: String = ""
    @State var password: String = ""
    
    @EnvironmentObject var signinviewmodel : AuthenticationVM
    
    var body: some View {

NavigationView {
            ZStack {
                    
                    Color("Primary").ignoresSafeArea()
                    
                    VStack
                    {
                        LogoWithName()
                            
                        TextField("Hospital ID", text: $username)
                            .autocapitalization(.none)
                            .disableAutocorrection(true)
                            .frame(width: 280, height: 25, alignment: .center)
                            .padding()
                            .background()
                            .cornerRadius(5.0)
                            .padding(.bottom, 20)
                            
                        
                        SecureField("Password", text: $password)
                            .frame(width: 280, height: 25, alignment: .center)
                            .padding()
                            .background()
                            .cornerRadius(5.0)
                            .padding(.bottom,30)
                        
                        Text("Forgot Password?")
                            .multilineTextAlignment(.trailing)
                        
                        HStack {
//
//                            Button(action: {print("Sign up Button tapped")})
//                            {
//                            SignupButton()
//                            }.padding(.horizontal, 40)
                            
                            NavigationLink(
                                destination: SignUp(username: $username, password: $password), label: {
                                    SignupButton()
                                }
                            ).padding()
                            
                            NavigationLink(isActive: $signinviewmodel.signedIn, destination:{HomeScreen()}, label: {Button(action: {
                            guard !username.isEmpty, !password.isEmpty else{
                                return
                            }
                                signinviewmodel.signIn(email: username, password: password)
                                
                            }, label: {
                                Login()
                                
                            })
                                .padding()})
                        }
                        
                        Text("Don't have an account?")
                            .padding()
                            .font(.footnote)
                        
                    }.padding()
            }
        }
    }
}


struct SignIn_Previews: PreviewProvider {
    static var previews: some View {
        
        SignIn()
            .environmentObject(AuthenticationVM())
            .previewInterfaceOrientation(.portrait)
       
    }
}




//// Plain button
//struct Login: View {
//    var body: some View {
//        Text("Login")
//            .font(.headline)
//            .foregroundColor(Color("Background"))
//            .padding(EdgeInsets(top: 9, leading: 30, bottom: 9, trailing: 30))
//            .background(Color("Secondary"))
//            .cornerRadius(5.0)
//    }
//}

struct LogoWithName: View {
    var body: some View {
        VStack
        {
            Image("KOS_LogoTransparent").resizable(resizingMode:.stretch)
                .background(Color("Primary"))
                .frame(maxWidth:100,maxHeight: 100)
            
            
            Text("KOS").font(.largeTitle).foregroundColor(.white)
        }
    }
}

