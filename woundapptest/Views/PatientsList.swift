//
//  PatientsList.swift
//  woundapptest
//
//  Created by Oluwatamilore Adenipekun on 16/01/2022.
//

import SwiftUI
import SDWebImageSwiftUI


let testData = [Patients(firstName: "Jane", lastName: "Doe", middleName: "", gender: "Female", height: "163", dateOfBirth: "05/21/1999", weight: "50",MRN:"#123456789",phone:"+ 202 555 0191",email:"janedoe22@gmail.com",address: "2971 Canis Heights Drive",emergencyContact: "+ 292 555 0191 Paul Smith",sepsisSurvey: [true,false,false,true,false,false,true],bradenScore: "15 - MILD RISK",bs:["1","3","3","4","2","2"],patientHistory: ["Yes","Yes","No","No","Yes","No","No","Yes","No","Single"],wounds: [Wound(woundName: "Pressure Ulcer", stage: "Stage 2", position: "Left toe"),Wound(woundName: "Pressure Ulcer", stage: "Stage 2", position: "Shoulder"),Wound(woundName: "Trauma Hematoma", stage: "Stage 1", position: "Calf")])]

struct PatientsList: View {
    
    @ObservedObject private var viewmodel = PatientsListVM()
    
    var body: some View {
        List{
            ForEach(viewmodel.patients){ patient in
                NavigationLink(destination: PatientProfile(patient:patient), label: {
                        HStack {
                            WebImage(url:viewmodel.url)
                                .resizable()
                                .frame(width: 50, height: 50, alignment: .leading) 
                                .clipShape(Circle())
                            VStack(alignment:.leading) {
                            HStack {
                                Text(patient.firstName)
                                Text(patient.lastName)
                                //Text(patient.id ?? "")
                                //NavigationLink(destination: HomeScreen(), label:{ Text("GO")})
                            }.font(.title2)
            
                            Text(patient.gender)
                            }
                        }})
        
                }
        }.listStyle(.plain)
            .onAppear(){
                self.viewmodel.fetchData()
                self.viewmodel.fetchImage(id:"RIqYrfgzwV6wrVinicyH")
            }
            
    }
}

struct PatientsList_Previews: PreviewProvider {
    static var previews: some View {
        PatientsList()
            .environmentObject(PatientsListVM())
    }
}
