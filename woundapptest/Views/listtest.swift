//
//  listtest.swift
//  woundapptest
//
//  Created by Oluwatamilore Adenipekun on 22/01/2022.
//

import SwiftUI

//let testData = [Patients(firstName: "Bisi", lastName: "Olu", middleName: "A", gender: "M", height: "1.22", dateOfBirth: "2/12/1990", weight: "87"),
//                Patients(firstName: "Wole", lastName: "Akin", middleName: "Q", gender: "F", height:" 1.74", dateOfBirth: "1/20/1967", weight: "300")]
//let testData = [Patients(firstName: "Bisi", lastName: "Olu", middleName: "A", gender: "M", height: "1.22", dateOfBirth: "2/12/1990", weight: "87",image: "0d4218784ff8f257b72fda920b43d7e7"),
//                Patients(firstName: "Wole", lastName: "Akin", middleName: "Q", gender: "F", height:" 1.74", dateOfBirth: "1/20/1967", weight: "300", image: "0a5c7e16e19f41fe66915abc274a98d7")]
struct listtest: View {
    var body: some View {
        
        List{
         ForEach(testData){ patient in
             NavigationLink(destination: HomeScreen(), label: {
                 HStack {
//                     Image(patient.image)
//                         .resizable()
//                         .frame(width: 50, height: 50, alignment: .leading)
//                         .clipShape(Circle())
                     VStack(alignment:.leading) {
                     HStack {
                         Text(patient.firstName)
                         Text(patient.lastName)
                         //NavigationLink(destination: HomeScreen(), label:{ Text("GO")})
                     }.font(.title2)

                     Text(patient.gender)
                     }
                 }
                 
             })

         }
         }.listStyle(.plain)
    }
}

struct listtest_Previews: PreviewProvider {
    static var previews: some View {
        listtest()
    }
}
