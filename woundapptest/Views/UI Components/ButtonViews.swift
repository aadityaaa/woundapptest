//
//  ButtonViews.swift
//  woundapptest
//
//  Created by Oluwatamilore Adenipekun on 31/12/2021.
//

import SwiftUI

struct ButtonViews: View {
    
    var body: some View {
        Text("HELLO")
    }
}

struct ButtonViews_Previews: PreviewProvider {
    static var previews: some View {
        ButtonViews()
.previewInterfaceOrientation(.portrait)
    }
}

struct HomescreenButton: View {
    
    let text : String
    let iconname : String
    let iconforegroundcolor : String
    
    var body: some View {
        VStack {
            
            Image(systemName: iconname)
                .renderingMode(.original)
                .foregroundColor(Color(iconforegroundcolor))
                .font(.system(size: 40))
            
            Text(text)
                .foregroundColor(Color("Tile Text"))
                .background(Color("Background"))
                
        }.frame(idealWidth: 170, maxWidth: 170, idealHeight: 100, maxHeight: 100, alignment: .center)
            .overlay(RoundedRectangle(cornerRadius: 10)
                    .stroke(Color("Tile Text"), lineWidth: 1))
    }
}

struct ScanQRButton: View {
    var body: some View {
        Text("Scan QR Code \(Image(systemName: "qrcode"))")
            .font(.headline)
            .frame(idealWidth: 120, maxWidth: 150, idealHeight: 50, maxHeight: 52, alignment: .center)
            .foregroundColor(Color("Primary"))
            .background(Color("Background"))
            .cornerRadius(5)
            .overlay(RoundedRectangle(cornerRadius: 5)
                        .stroke(Color("Primary"), lineWidth: 1))
    }
}

struct Login: View {
    var body: some View {
        Text("Login")
            .font(.headline)
            .frame(idealWidth: 120, maxWidth: 150, idealHeight: 50, maxHeight: 52, alignment: .center)
            .foregroundColor(Color("Primary"))
            .background(Color("Secondary"))
            .cornerRadius(5)
            .overlay(RoundedRectangle(cornerRadius: 5)
                        .stroke(Color("Secondary"), lineWidth: 1))
    }
}

struct SignupButton: View {
    var body: some View {
        Text("Sign up")
            .font(.headline)
            .frame(idealWidth: 120, maxWidth: 150, idealHeight: 50, maxHeight: 52, alignment: .center)
            .foregroundColor(Color("Secondary"))
            .background(Color("Background"))
            .cornerRadius(5)
            .overlay(RoundedRectangle(cornerRadius: 5)
                        .stroke(Color("Secondary"), lineWidth: 1))
    }
}

struct MediumButton: View {
    var text: String
    var fgcolor: String
    var bgcolor: String
    var width: CGFloat
    var height:CGFloat
    var radius: CGFloat = 5.0
    var body: some View {
        Text(text)
            .font(.headline)
            .frame(idealWidth: width, maxWidth: 150, idealHeight: height, maxHeight: 80, alignment: .center)
            .foregroundColor(Color(fgcolor))
            .background(Color(bgcolor))
            .cornerRadius(radius)
            .overlay(RoundedRectangle(cornerRadius: radius)
                        .stroke(Color(fgcolor), lineWidth: 1))
    }
}
