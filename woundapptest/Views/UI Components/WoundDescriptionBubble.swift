//
//  WoundDescriptionBubble.swift
//  woundapptest
//
//  Created by Carlos Morales III on 3/17/22.
//

import SwiftUI

struct WoundDescriptionBubble: View {
//	var woundDescriptionBubbleViewModel = WoundDescriptionBubbleViewModel()
	@State var containerWidth: CGFloat = 162
	@State var containerHeight: CGFloat = 74
    
	var body: some View {
		ZStack {
			RoundedRectangle(cornerRadius: 5)
				.fill(Color("Primary"))
				.frame(width: containerWidth, height: containerHeight, alignment: .center)
			
			HStack {
				VStack(alignment: .leading) {
					Text("Hematoma")
						.bold()
						.padding(.top, 10)
					Spacer()
					Text("Stage 2")
					Spacer()
					Text("Calf")
						.padding(.bottom, 10)
				}
				.foregroundColor(.white)
				.font(.system(size: 12))
			}
			.frame(width: containerWidth, height: containerHeight, alignment: .leading)
			.padding(.leading, 20)
		}
    }
}

struct WoundDescriptionBubble_Previews: PreviewProvider {
    static var previews: some View {
        WoundDescriptionBubble()
    }
}
