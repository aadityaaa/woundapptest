//
//  Wounds.swift
//  woundapptest
//
//  Created by stella on 3/2/22.
//

import SwiftUI

struct Wounds: View {
    
    var patient:Patients
    
    var body: some View {
        VStack(alignment: .leading){
            HStack {
                NavigationLink {
                    ViewWound()
                } label: {
                    Group {
                        Text("View All").foregroundColor(.white).padding(10)
                    }.background(Color("Icon_Scanwound")).cornerRadius(5).frame(maxWidth: .infinity, alignment: .trailing)
                }
            }.padding(.horizontal)
            ForEach (0..<patient.wounds.count){ index in
                VStack {
                    HStack(spacing:50){
                        VStack(alignment:.leading){
                            Group{
                                Text(patient.wounds[index].woundName).bold().fixedSize().frame(maxWidth: .infinity, alignment: .leading).font(.title3)
                                Text(patient.wounds[index].stage)
                                Text(patient.wounds[index].position)
                            }.frame(height: 12)
                        }
                        
                        Group{
                            Button {
                                
                            } label: {
                                Text("View").padding(5).padding(.horizontal).foregroundColor(Color("Icon_Scanwound")).overlay(
                                    RoundedRectangle(cornerRadius: 5)
                                        .stroke(Color("Icon_Scanwound"), lineWidth: 1)
                                )
                            }
                        }.frame(maxWidth: .infinity, alignment: .trailing)
                    }
                    Divider()
                }.padding()
            }
        }.padding()
    }
}

struct Wounds_Previews: PreviewProvider {
    static var previews: some View {
        Wounds(patient: testData[0])
    }
}
