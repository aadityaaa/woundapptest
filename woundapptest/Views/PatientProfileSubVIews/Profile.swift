//
//  Profile.swift
//  woundapptest
//
//  Created by stella on 3/3/22.
//

import SwiftUI

struct Profile: View {
    
    var patient:Patients
    
    var body: some View {
        VStack{
            HStack(spacing:40){
                HStack(alignment: .firstTextBaseline) {
                    //WebImage(url:viewmodel.url)
                    Image("PaulSmith").resizable().frame(width: 130, height: 130).clipShape(Circle())
                    Image(systemName: "camera.fill").frame(maxWidth: .leastNonzeroMagnitude, alignment: .bottom)
                }
                VStack(alignment: .leading){
                    Text(patient.firstName + " " + patient.lastName)
                        .font(.largeTitle)
                    Text("D.O.B.: " + patient.dateOfBirth)
                    Text("Age: " + "22")
                    Text("Gender: " + patient.gender)
                    Text("MRN: " + patient.MRN)
                }
            }
            HStack{
                Spacer()
                Group {
                    Button {
                        
                    } label: {
                        Image(systemName:"pencil").foregroundColor(.white)
                        Text("edit")
                            .padding(.trailing)
                            .foregroundColor(.white)
                            .padding(5)
                    }.background(Color("Icon_Patients"))
                        .cornerRadius(5)
                        .padding()
                }
            }
        }
    }
}

struct Profile_Previews: PreviewProvider {
    static var previews: some View {
        Profile(patient: testData[0])
    }
}
