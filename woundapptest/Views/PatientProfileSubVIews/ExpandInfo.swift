//
//  ExpandInfo.swift
//  woundapptest
//
//  Created by stella on 3/3/22.
//

import SwiftUI

struct ExpandInfo: View {
    
    @Binding var expanding:Bool
    var information:String
    
    var body: some View {
        VStack {
            HStack{
                Image(systemName: expanding ? "chevron.up":"chevron.down")
                Text(information).font(.system(size: 20))
            }.frame(maxWidth: .infinity, alignment: .leading).padding(.horizontal)
            Divider()
        }
    }
}

struct ExpandInfo_Previews: PreviewProvider {
    static var previews: some View {
        ExpandInfo(expanding:.constant(false),information: "Contact Details")
    }
}
