//
//  ContactDetails.swift
//  woundapptest
//
//  Created by stella on 3/3/22.
//

import SwiftUI

struct ContactDetails: View {
    
    var patient:Patients
    
    var body: some View {
        VStack(alignment:.leading,spacing:15){
            HStack{
                Image(systemName: "phone")
                Text(patient.phone)
            }.padding(.horizontal)
            Divider()
            HStack{
                Image(systemName:"envelope")
                Text(patient.email)
            }.padding(.horizontal)
            Divider()
            HStack{
                Image(systemName:"mappin")
                Text(patient.address)
            }.padding(.horizontal)
            Divider()
            HStack{
                Image(systemName: "staroflife")
                Text(patient.emergencyContact)
            }.padding(.horizontal)
        }.padding(.horizontal)
    }
}

struct ContactDetails_Previews: PreviewProvider {
    static var previews: some View {
        ContactDetails(patient:testData[0])
    }
}
