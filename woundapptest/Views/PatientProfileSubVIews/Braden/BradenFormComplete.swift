//
//  BradenFormComplete.swift
//  woundapptest
//
//  Created by stella on 3/5/22.
//

import SwiftUI

struct BradenFormComplete: View {
    
    @Binding var newScore:Bool
    @Binding var opacity:Double
    
    var body: some View {
        ZStack{
            if newScore {
                Rectangle().foregroundColor(.white)
                VStack{
                    HStack(spacing:18){
                        Text("                    ")
                        Text("1").padding(.horizontal,4)
                        Text("2").padding(.horizontal,4)
                        Text("3").padding(.horizontal,4)
                        Text("4").padding(.horizontal,4)
                    }
                    BradenForm(braden: "Sensory perception")
                    BradenForm(braden: "Moisture")
                    BradenForm(braden: "Activity")
                    BradenForm(braden: "Mobility")
                    BradenForm(braden: "Nutrition")
                    BradenForm(braden: "Friction and shear")
                    HStack{
                        Spacer()
                        Button {
                            newScore = false
                            opacity = 1.0
                        } label: {
                            Text("Cancel").foregroundColor(Color("Icon_Patients")).padding(8).overlay(
                                RoundedRectangle(cornerRadius: 5)
                                    .stroke(Color("Icon_Patients"), lineWidth: 1)
                            )
                        }
                        Group {
                            Button {
                                
                            } label: {
                                Text("Add").foregroundColor(.white).padding(8)
                            }
                        }.background(Color("Icon_Patients")).cornerRadius(5).padding(5)
                        
                    }.padding(.horizontal,10)
                    
                }.padding()//.fixedSize()
            }
        }.fixedSize().opacity(1.0)
        

    }
}

struct BradenFormComplete_Previews: PreviewProvider {
    static var previews: some View {
        BradenFormComplete(newScore:.constant(true),opacity: .constant(1.0))
    }
}
