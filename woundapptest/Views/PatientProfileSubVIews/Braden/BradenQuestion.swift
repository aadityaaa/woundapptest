//
//  BradenQuestion.swift
//  woundapptest
//
//  Created by stella on 3/4/22.
//

import SwiftUI

struct BradenQuestion: View {
    
    var question:String
    var mesg:String
    var score:String
    
    var body: some View {
        //GeometryReader { geo in
            HStack {
                //Spacer()
                VStack(alignment:.leading,spacing:2){
                    Text(question).font(.title2)
                    Text(mesg)
                }
                Spacer()
                Text(score).font(.largeTitle)
                //Spacer()
            }.padding(5).padding(.horizontal)//.frame(width:geo.size.width-16,height:60)
            .overlay(
                RoundedRectangle(cornerRadius: 10)
                    .stroke(Color("Tile Text"),lineWidth: 1)//.padding()
        )
       // }
        
    }
}

struct BradenQuestion_Previews: PreviewProvider {
    static var previews: some View {
        BradenQuestion(question:"Sensory perception",mesg: "Completely limited",score:"1")
    }
}
