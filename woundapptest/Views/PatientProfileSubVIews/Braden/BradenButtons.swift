//
//  BradenButtons.swift
//  woundapptest
//
//  Created by stella on 3/5/22.
//

import SwiftUI

struct BradenButtons: View {
    
    let id: Int
    @Binding var currentSelectionId: Int
    
    var body: some View {
        Button {
            self.currentSelectionId = self.id
        } label: {
            //(systemName: expanding ? "chevron.up":"chevron.down")
            Image(systemName: (id == currentSelectionId) ? "circle.fill":"circle").foregroundColor(.black)
        }
    }
}

struct BradenButtons_Previews: PreviewProvider {
    static var previews: some View {
        BradenButtons(id:0,currentSelectionId: .constant(0))
    }
}
