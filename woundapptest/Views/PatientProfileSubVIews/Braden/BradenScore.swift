//
//  BradenScore.swift
//  woundapptest
//
//  Created by stella on 3/4/22.
//

import SwiftUI

struct BradenScore: View {
    
    var patient:Patients
    @Binding var newScore:Bool
    @Binding var opacity:Double
    
    var body: some View {
        VStack(alignment:.center,spacing:10){
            Text(patient.bradenScore).bold().font(.title).foregroundColor(Color("Icon_Scanwound"))
            BradenQuestion(question: "Sensory perception", mesg: "Completely limited", score: patient.bs[0])
            BradenQuestion(question: "Moisture", mesg: "Occasionally moist", score: patient.bs[1])
            BradenQuestion(question: "Activity", mesg: "Walks occasionally", score: patient.bs[2])
            BradenQuestion(question: "Mobility", mesg: "no limitation", score: patient.bs[3])
            BradenQuestion(question: "Nutrition", mesg: "Probably inadequate", score: patient.bs[4])
            BradenQuestion(question: "Friction and shear", mesg: "Potential problem", score: patient.bs[5])
            Group{
                Button {
                    newScore.toggle()
                    opacity = 0.5
                } label: {
                    Text("Calculate new score").foregroundColor(.white)
                }
            }.padding(10).background(Color("Icon_Patients")).cornerRadius(5).padding(.vertical)

        }.padding()
    }
}

struct BradenScore_Previews: PreviewProvider {
    static var previews: some View {
        BradenScore(patient:testData[0],newScore: .constant(false),opacity: .constant(1.0))
    }
}
