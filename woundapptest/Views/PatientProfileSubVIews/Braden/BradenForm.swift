//
//  BradenForm.swift
//  woundapptest
//
//  Created by stella on 3/4/22.
//

import SwiftUI

struct BradenForm: View {
    
    //let id: Int
    var braden:String
    @State var currentSelectionId: Int = 0
    
    var body: some View {
        HStack {
            Text(braden).frame(width:100).multilineTextAlignment(.center)//.frame(maxWidth: .infinity, alignment: .leading)
            BradenButtons(id: 1, currentSelectionId: $currentSelectionId).padding(.horizontal,4)
            BradenButtons(id: 2, currentSelectionId: $currentSelectionId).padding(.horizontal,4)
            BradenButtons(id: 3, currentSelectionId: $currentSelectionId).padding(.horizontal,4)
            BradenButtons(id: 4, currentSelectionId: $currentSelectionId).padding(.horizontal,4)
        }.padding()
    }
}

struct BradenForm_Previews: PreviewProvider {
    static var previews: some View {
        BradenForm(braden:"Sensory perception", currentSelectionId:0)
    }
}
