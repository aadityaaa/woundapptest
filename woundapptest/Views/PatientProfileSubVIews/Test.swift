//
//  Test.swift
//  woundapptest
//
//  Created by stella on 3/4/22.
//

import SwiftUI

struct Test: View {
    var body: some View {
        VStack{
            Text("Updated 3 days ago").frame(maxWidth: .infinity, alignment: .trailing).padding().font(.subheadline).foregroundColor(Color("Icon_Scanwound"))
            HStack(spacing:50){
                VStack(alignment: .leading) {
                    Group{
                        Text("CBC")
                        Text("Glucose")
                        Text("Creatinine")
                        Text("Urinalysis")
                    }.frame(height: 30)
                }
                Divider()
                VStack {
                    Group{
                        ForEach (0..<4,id:\.self){ index in
                            Text("%")
                        }
                    }.frame(height: 30)
                }
            }.fixedSize().padding()
        }
    }
}

struct Test_Previews: PreviewProvider {
    static var previews: some View {
        Test()
    }
}
