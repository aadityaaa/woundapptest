//
//  SurveyQuestion.swift
//  woundapptest
//
//  Created by stella on 3/2/22.
//

import SwiftUI

struct SurveyQuestion: View {
    
    var question:String
    var num:Int
    var patient:Patients
    
    var body: some View {
        VStack{
            Text(question).fixedSize(horizontal: false, vertical: true).frame(maxWidth: .infinity, alignment: .leading)
            HStack{
                Button {
                    
                } label: {
                    HStack{
                        Image(systemName: patient.sepsisSurvey[num] ? "circle.fill":"circle").foregroundColor(Color("Icon_Patients"))
                        Text("Yes").foregroundColor(Color("Tile Text"))
                    }
                }
                Button {
                    
                } label: {
                    HStack{
                        Image(systemName: patient.sepsisSurvey[num] ? "circle":"circle.fill").foregroundColor(Color("Icon_Patients"))
                        Text("No").foregroundColor(Color("Tile Text"))
                    }
                }
            }.padding()
        }.padding(.horizontal)
    }
}

struct SurveyQuestion_Previews: PreviewProvider {
    static var previews: some View {
        SurveyQuestion(question: "1. Unexplained altered mental status, confusion or disorientation?",num:0,patient:testData[0])
    }
}
