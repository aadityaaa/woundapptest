//
//  DailyCheckIn.swift
//  woundapptest
//
//  Created by stella on 3/2/22.
//

import SwiftUI

struct DailyCheckIn: View {
    
    @Binding var show:Bool
    //@Binding var opacity:Double
    
    var body: some View {
        VStack{
            HStack{
                Text("Status: ").foregroundColor(Color("Icon_Scanwound"))
                Text("SEPSIS ALERT").foregroundColor(.red)
            }
            Group {
                Button {
                    show.toggle()
                    //opacity = 0.5
                } label: {
                    Text("Start sepsis survey").foregroundColor(.white).padding(10)
                }
            }.background(Color("Icon_Scanwound")).cornerRadius(5)
        }.padding()
    }
}

struct DailyCheckIn_Previews: PreviewProvider {
    static var previews: some View {
        DailyCheckIn(show:.constant(false))
    }
}
