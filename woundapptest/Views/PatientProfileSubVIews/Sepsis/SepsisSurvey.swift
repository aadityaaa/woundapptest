//
//  SepsisSurvey.swift
//  woundapptest
//
//  Created by stella on 3/2/22.
//

import SwiftUI

struct SepsisSurvey: View {
    
    @Binding var show:Bool
    var patient:Patients
    
    var body: some View {
        GeometryReader{ geo in
            ZStack(alignment: .center){
                if show{
                    Rectangle().foregroundColor(.white)
                    VStack{
                        SurveyQuestion(question: "1. Unexplained altered mental status, confusion or disorientation?",num:0,patient: patient)
                        SurveyQuestion(question: "2. Shorness of breath?",num:1,patient: patient)
                        SurveyQuestion(question: "3. Hypotension?",num:2,patient: patient)
                        SurveyQuestion(question: "4. Signs of wound infection?",num:3,patient: patient)
                        SurveyQuestion(question: "5. Heart rate > 140/min or < 40/min?",num:4,patient: patient)
                        SurveyQuestion(question: "6. Extreme pain or discomfort?",num:5,patient: patient)
                        SurveyQuestion(question: "7. Fever, leukocytosis, patient shivering or feeling very cold, clammy or sweathy skin?",num:6,patient: patient)
                        HStack{
                            Spacer()
                            Button {
                                show = false
                                //opacity = 1.0
                            } label: {
                                Text("Cancel").foregroundColor(Color("Icon_Scanwound")).padding(8).overlay(
                                    RoundedRectangle(cornerRadius: 5)
                                        .stroke(Color("Icon_Scanwound"), lineWidth: 1)
                                )
                            }
                            Group {
                                Button {
                                    
                                } label: {
                                    Text("Add").foregroundColor(.white).padding(8)
                                }
                            }.background(Color("Icon_Scanwound")).cornerRadius(5).padding(5)
                            
                        }.padding(.horizontal)
                    }
                }
            }.frame(width: geo.size.width-20, height: geo.size.height-20, alignment: .center).fixedSize().position(x: geo.frame(in: .local).midX, y: geo.frame(in: .local).midY)
        //.fixedSize(horizontal: false, vertical: true)
        }
    }
}

struct SepsisSurvey_Previews: PreviewProvider {
    static var previews: some View {
        SepsisSurvey(show:.constant(true),patient: testData[0])
    }
}
