//
//  PatientHistory.swift
//  woundapptest
//
//  Created by stella on 3/4/22.
//

import SwiftUI

struct PatientHistory: View {
    
    var patient:Patients
    
    var body: some View {
        VStack {
            Group {
                Button {
                    
                } label: {
                    Text("Details").foregroundColor(.white)
            }
            }.padding(10).background(Color("Icon_Scanwound")).cornerRadius(5).padding(.vertical).frame(maxWidth: .infinity, alignment: .trailing).padding(.horizontal)
            HStack(spacing:40) {
                VStack(alignment: .leading) {
                    Text("Health History").foregroundColor(Color("Tile Text"))
                    Group {
                        Text("Diabetes?")
                        Text("High Cholesterol?")
                        Text("High Blood Pressure?")
                        Text("Other conditions?")
                    }.frame(height: 30)
                    Text("Social History").foregroundColor(Color("Tile Text"))
                    Group{
                        Text("Smoker?")
                        Text("Vaping?")
                        Text("Drinking alcohol?")
                        Text("Drugs?")
                        Text("Physical active?")
                        Text("Martial status?")
                    }.frame(height: 30)
                }.fixedSize(horizontal: true, vertical: false)
                Divider()
                VStack (alignment:.leading){
                    Text(" ")
                    Group{
                        HStack{
                            Text(patient.patientHistory[0])
                            Divider()
                            Text("Type 2")
                        }
                        Text(patient.patientHistory[1])
                        Text(patient.patientHistory[2])
                        Text(patient.patientHistory[3])
                    }.frame(height: 30)
                    Text(" ")
                    Group{
                        Text(patient.patientHistory[4])
                        Text(patient.patientHistory[5])
                        Text(patient.patientHistory[6])
                        Text(patient.patientHistory[7])
                        Text(patient.patientHistory[8])
                        Text(patient.patientHistory[9])
                    }.frame(height: 30)
                }
            }.fixedSize().padding()
        }
    }
}

struct PatientHistory_Previews: PreviewProvider {
    static var previews: some View {
        PatientHistory(patient: testData[0])
    }
}
