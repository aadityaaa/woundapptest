//
//  woundapptestApp.swift
//  woundapptest
//
//  Created by Oluwatamilore Adenipekun on 24/12/2021.
//

import SwiftUI
import Firebase

@main
struct woundapptestApp: App {

    @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate
    
    var body: some Scene {
        WindowGroup {
            let signinvm = AuthenticationVM()
            let saveimagesvm = SaveImagesVM()
            ContentView()
                .environmentObject(signinvm)
                .environmentObject(saveimagesvm)
//			WoundHomeAssignment()
            
        }
    }
}

class AppDelegate: NSObject, UIApplicationDelegate {
  func application(_ application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
    print("Application is starting up. ApplicationDelegate didFinishLaunchingWithOptions.")
    FirebaseApp.configure()
    return true
  }
}
